cmake_minimum_required(VERSION 3.13)
project(demoGAN)

cmake_minimum_required(VERSION 2.8)
set(CMAKE_CONFIGURATION_TYPES "Debug" CACHE STRING "" FORCE)

find_package(OpenCV  REQUIRED)
FIND_PACKAGE(Qt5 REQUIRED COMPONENTS Widgets Core)
FIND_PACKAGE(Boost REQUIRED filesystem system)

aux_source_directory(src DEMO_SRC)
aux_source_directory(capture_wrapper CAPTURE_SRC)

include_directories (./capture_wrapper)
include_directories (${Qt5Widgets_INCLUDE_DIRS})
include_directories(${Boost_INCLUDE_DIRS})


#OPENCV
include_directories (${OpenCV_INCLUDE_DIRS})


link_directories (${OpenCV_LIBRARY_DIRS})

ADD_EXECUTABLE(demo ${DEMO_SRC} ${CAPTURE_SRC})
TARGET_LINK_LIBRARIES(demo  ${OpenCV_LIBS} ${Boost_LIBRARIES} Qt5::Gui Qt5::Core)
set(CMAKE_CXX_FLAGS "-std=c++11")

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -std=c++11")