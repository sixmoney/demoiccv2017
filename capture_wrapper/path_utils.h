/*
 * path_utils.h
 *
 *  Created on: Oct 7, 2014
 *      Author: seide
 */

#ifndef PATH_UTILS_H_
#define PATH_UTILS_H_

#include <boost/filesystem.hpp>
#include <string>


namespace bfs = boost::filesystem;

bfs::path catToPathBeforeExtension(bfs::path& p, const std::string tag );
bfs::path replaceExtension(bfs::path& p,const std::string ext);
bfs::path catDirBeforeFile(const bfs::path& p, const std::string dir);




#endif /* PATH_UTILS_H_ */
