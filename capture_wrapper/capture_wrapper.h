/*
 * directory_capture.h
 *
 *  Created on: Jun 4, 2015
 *      Author: seide
 */

#ifndef CAPTURE_WRAPPER_H
#define CAPTURE_WRAPPER_H
#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>
class CaptureWrapper {
	cv::VideoCapture* vc;
	std::vector<std::string> image_files;

public:
	CaptureWrapper(std::string path, cv::Size capture_size);
	CaptureWrapper(int camera_id, cv::Size capture_size);
	bool isStream();
    virtual bool grab();
	virtual bool retrieve(cv::Mat& image);
//	virtual CaptureWrapper& operator >> ( cv::Mat& image);
	virtual bool read(cv::Mat& image);
	virtual ~CaptureWrapper();
private:
	cv::Size capture_size;
	void read_raw(std::string path,cv::Mat& frame);
};




#endif /* DIRECTORY_CAPTURE_DIRECTORY_CAPTURE_H_ */
