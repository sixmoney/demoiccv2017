/*
 * path_utils.cpp
 *
 *  Created on: Oct 7, 2014
 *      Author: seide
 */




#include "path_utils.h"
#include <boost/filesystem.hpp>
#include <string>
namespace bfs = boost::filesystem;

bfs::path catToPathBeforeExtension(bfs::path& p, const std::string tag ){ //< return a path where the filename is augmented with tag: /path/file.jpg -> /path/file_tag.jpg
	bfs::path augmented = p.branch_path() / bfs::path(p.stem().string() + tag + p.extension().string());
	return augmented;
}

bfs::path catDirBeforeFile(const bfs::path& p, const std::string dir){
	bfs::path augmented = p.branch_path() / bfs::path(dir) / p.filename();
	return augmented;
}

bfs::path replaceExtension(bfs::path& p,const std::string ext){
	return p.branch_path() / bfs::path(p.stem().string() + ext);
}
