/*
 * capture_wrapper.cpp
 *
 *  Created on: Jun 4, 2015
 *      Author: seide
 */

#include "capture_wrapper.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <opencv2/opencv.hpp>
#include <string>
#include "path_utils.h"
namespace bfs = boost::filesystem;
CaptureWrapper::CaptureWrapper(int camera_id, cv::Size capture_size ){
	this->vc=new cv::VideoCapture(camera_id);
	this->capture_size=capture_size;
}
bool CaptureWrapper::isStream(){
	return !(this->image_files.empty());
}
void CaptureWrapper::read_raw(std::string path,cv::Mat& frame){
	std::vector<std::string> parts;
	boost::split(parts,path,boost::is_any_of("."));
	std::vector<std::string> wh;

	boost::split(wh,parts[1],boost::is_any_of("x"));

	size_t width = atoi(wh[0].c_str());
	size_t height = atoi(wh[1].c_str());
	std::cout << "width: " << width
			<< "height: " <<height << std::endl;

	FILE* fp = fopen(path.c_str(),"rb");
	frame=cv::Mat(height,width,CV_8UC1);
	fread(frame.data,sizeof(uchar)*width*height,1,fp);

	fclose(fp);
}

CaptureWrapper::CaptureWrapper(std::string _path , cv::Size capture_size){
	this->capture_size=capture_size;
	bfs::path p(_path);
	  bfs::directory_iterator end_dir;
	if (!bfs::is_directory(p)){
		vc=new cv::VideoCapture(_path);
	} else {
		vc=NULL;

		std::vector<bfs::path> file_vector;        // store paths,
		copy(bfs::directory_iterator(p), bfs::directory_iterator(), std::back_inserter(file_vector));
		struct compare_strings_reversed {
			bool operator()(bfs::path const &a, bfs::path const &b) const { return a.string().compare(b.string())>0; }
		};
		sort(file_vector.begin(), file_vector.end(),compare_strings_reversed());


		//for (bfs::directory_iterator dir_it(p); dir_it != end_dir; dir_it++){
		for(auto dir_it = file_vector.begin(); dir_it != file_vector.end();dir_it++){
				if ( !bfs::is_directory(*dir_it)){
					std::string ext_string =  (*dir_it).extension().string();
					boost::algorithm::to_lower(ext_string);
					std::cout << "reading " << (*dir_it).string() << std::endl;
					if (ext_string.compare(".jpg")==0 || ext_string.compare(".png")==0 || ext_string.compare(".raw")==0 ){
						this->image_files.push_back((*dir_it).string());
					}
				}
		}
	}
}
CaptureWrapper::~CaptureWrapper(){
	if (this->vc != NULL && this->vc->isOpened() ){
		delete (this->vc);
	}
}
bool CaptureWrapper::grab(){
	if(vc != NULL && vc->isOpened()){
		return vc->grab();
	}
	else if(image_files.size()>0)
		return true;
	else
		return false;
}
bool CaptureWrapper::read(cv::Mat& image){
	if(vc != NULL &&vc->isOpened()){
		vc->read(image);
		//cv::resize(image,image,capture_size);
		return true;
	} else {
		if(image_files.size()>0){
			std::string imfile = image_files.back();
			image_files.pop_back();
			if (imfile.substr(imfile.find_last_of(".") + 1) == "raw"){
				//std::cout << "reading raw image" <<std::endl;
				read_raw(imfile,image);
				cv::imshow("raw_image",image);cv::waitKey(0);
			}else {
				image = cv::imread(imfile);
				//cv::resize(image,image,capture_size);
			}
			if(image.rows>0) return true;
			else return false;
		} else return false;
	}
}
bool CaptureWrapper::retrieve(cv::Mat& image){
	if(vc != NULL && vc->isOpened()){
		return vc->retrieve(image);
		} else {
			if(image_files.size()>0){
				std::string imfile = image_files.back();
				//std::cout << imfile << std::endl;
				image_files.pop_back();
				if (imfile.substr(imfile.find_last_of(".") + 1) == "raw"){
					//std::cout << "reading raw image" <<std::endl;
					read_raw(imfile,image);

				}else {
					image = cv::imread(imfile);
				}
				if(image.rows>0) return true;
				else return false;

			} else return false;
		}
}



