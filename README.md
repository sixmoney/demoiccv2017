ICCV 2017 Demo
-------------
-------------

Demo for Compression Artifact Removal via Deep Generative Adversarial Networks, Leonardo Galteri, Lorenzo Seidenari, Marco Bertini, Alberto Del Bimbo.

Compiling:
-----------

```
mkdir build
cd build
cmake ..
```


Running:
-----------

```
build/demo -c=./videos/blade_runner_ending/compressed/ -r=./videos/blade_runner_ending/restored/
```