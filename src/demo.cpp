/*
 * demo.cpp
 *
 *  Created on: Aug 21, 2017
 *      Author: seide
 */

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <stdio.h>
#include <iostream>
#include <numeric>
#include "capture_wrapper.h"
#include <boost/filesystem.hpp>

#include <QtGui/qwindowdefs.h>
#include <QtGui/qfont.h>

#include <QtGui/qfontmetrics.h>



//#include <qt4/QtGui/qwindowdefs.h>
//#include <qt4/QtGui/qfont.h>

//#include <qt4/QtGui/qfontmetrics.h>



namespace bfs = boost::filesystem;
using namespace cv;
using namespace std;
static void help(char** argv)
{
	printf("\nShow Cases Artifact Removal using two videos"
			"Usage: \n"
			"%s -m=./videos \n\n",argv);
			//"%s  [--compressed_file]=<path to movie file or dir> [--restored_file]=<path to movie file or dir> \n\n",argv[0]);

}

const char* keys =
{
		"{m movie_dir | ./videos | path to dir with movies} "
		"{r record | false | start recording right away}"
//		"{c  compressed_file   |         	| path to compressed file or images}"
//		"{r  restored_file     |     		| path to restored file or images }"

};
bool paused;
void onMouseMoveZoom(int event, int x, int y, int, void* data){
	cv::Point& center = *(cv::Point*)data;
	if (event == EVENT_MOUSEMOVE){
		//std::cout << "onMouseMoveZoom: " << x << ", " << y <<  " paused: " << paused<< std::endl;

		center.x=x;
		center.y=y;

	}
	if (event == EVENT_MBUTTONDOWN){
		paused = !paused;
	}

}
int main(int argc, char** argv){
	paused = false;
	cv::Point center;

	CaptureWrapper *compressed_cap, *restored_cap, *compressed_cap_det, *restored_cap_det;
	cv::Mat compressed_frame_free, restored_frame_free,compressed_frame, restored_frame, compressed_frame_det, restored_frame_det;
	cv::Size capture_size(0,0);
	CommandLineParser parser(argc, argv, keys);
	std::string movie_directory = parser.get<string>("movie_dir");

	bfs::path p(movie_directory);
	std::vector<bfs::path> movie_vector;        // store paths,

	copy(bfs::directory_iterator(p), bfs::directory_iterator(), std::back_inserter(movie_vector));

	struct compare_strings {
		bool operator()(bfs::path const &a, bfs::path const &b) const { return a.string().compare(b.string())<0; }
	};
	sort(movie_vector.begin(), movie_vector.end(),compare_strings());
	std::cout << "Movies " << movie_vector.size() <<std::endl;
	bool recording=false; //flag to signal recording is ongoing
	bool start_recording=false; //flag to toggle recording
	if (parser.get<bool>("record")){
		start_recording=true;	
	}
	std::stringstream recording_filename_ss;
	int id_recording=0;
	bool show_help=true;
	int recorder_rows,recorder_cols;
	bool detections_on=false;
	cv::VideoWriter vw;
	for (size_t i=0; ; i=(i+1)%movie_vector.size()){
		bool detections_available = false;
		
		//stop recording if video is finished
		if (recording){
			recording=false;
			vw.release();
		}
		//read fps
		std::ifstream fps_file;
		bfs::path movie_path = (movie_vector[i]);
		std::cout << "Now playing: "<< i<<" " << movie_vector[i] << std::endl;
		string  fps_filename = movie_path.string()+"/fps.txt";
		fps_file.open(fps_filename);
		if ( !fps_file.is_open()  ){
			std::cout << "can not open: " << fps_filename << std::endl;
			continue;//skip this
		}

		float fps=0;
		fps_file >> fps;
		fps_file.close();

		string compressed_video_file = movie_path.string()+ "/compressed";
		string restored_video_file = movie_path.string()+ "/restored";
		string compressed_video_file_detections = movie_path.string()+ "/compressed_detections";
		string restored_video_file_detections = movie_path.string()+ "/restored_detections";


		compressed_cap = new CaptureWrapper(compressed_video_file, capture_size);
		restored_cap = new CaptureWrapper(restored_video_file, capture_size);
		compressed_cap_det = new CaptureWrapper(compressed_video_file_detections, capture_size);
		restored_cap_det = new CaptureWrapper(restored_video_file_detections, capture_size);


		detections_available = restored_cap_det->isStream() && compressed_cap_det->isStream();

				bool exit_loop = false;
		cv::namedWindow("composite",WINDOW_NORMAL);
		int frameno = 0;
		show_help=true;
		while(!exit_loop && compressed_cap->grab()) {

			if (show_help){
				displayOverlay("composite", "Spacebar: Skip to next movie/album\n"
											"Mouse Middle: pause\n"
											"Mouse Wheel: zoom\n"
											"Mouse Left Button: pick to pan zoomed image\n"
											"n: pause movie/next still\n"
											"F1: show this help\n",5000 );
				show_help=false;

			}

			cv::setMouseCallback("composite",onMouseMoveZoom,(void*)(&center));
			double t1 = cv::getTickCount();
			if (!paused){
						compressed_cap->grab();
						restored_cap->grab();
						compressed_cap->retrieve(compressed_frame_free);
						restored_cap->retrieve(restored_frame_free);

						compressed_cap_det->grab();
						restored_cap_det->grab();
						compressed_cap_det->retrieve(compressed_frame_det);
						restored_cap_det->retrieve(restored_frame_det);


						cout << "frameno: "<< frameno++ << endl;

			}
			if (fps==0)
				paused=true;



				cv::Mat composite = cv::Mat::zeros(restored_frame.rows,restored_frame.cols, CV_8UC3);
				//cv::Rect crop = cv::Rect(0,0,restored_frame.cols-1,restored_frame.rows-1);
				
				if(detections_on && detections_available){ //in case detections are on we make compressed_frame point to compressed_frame_det.
					compressed_frame = compressed_frame_det;
					restored_frame = restored_frame_det;
				} else {
					compressed_frame = compressed_frame_free;
					restored_frame = restored_frame_free;
				}

				composite = compressed_frame.clone();
				cv::Rect roi = cv::Rect(0,0,center.x,composite.rows);
				//cout << "roi:" << roi << endl;
				roi = roi & cv::Rect(0,0,restored_frame.cols, restored_frame.rows);
				if (center.x>0){
					restored_frame(roi).copyTo(composite(roi));
				}
				double font_size = composite.rows*(0.025);
				QFont font("arial", int(font_size));
				QFontMetrics fm(font);
				int res_width = fm.width("Restored");
				int res_height = fm.height();
				cv::Rect text_background_roi(0,0,composite.cols,res_height*1.1);
				vector<cv::Mat> channels;
				cv::split(composite,channels);
				channels[0](text_background_roi)-=50;
				channels[1](text_background_roi)-=50;
				channels[2](text_background_roi)-=50;
				merge(channels,composite);
				cv::line(composite,cv::Point(center.x,0),cv::Point(center.x,composite.rows),CV_RGB(255,255,255),1);

				int c=0;
				cv::addText(composite,"Restored",cv::Point(center.x-res_width-10,res_height),"Arial",font_size,cv::Scalar(255,255,255));
				cv::addText(composite,"Compressed",cv::Point(center.x+10,res_height),"Arial",font_size,cv::Scalar(255,255,255));

				//c=cv::waitKey(1);
				if (!recording && start_recording){
					id_recording++;
					recording_filename_ss.str("");
					recording_filename_ss.clear();
					recording_filename_ss << "recording_" << id_recording << ".avi";
					vw = cv::VideoWriter(   recording_filename_ss.str(), cv::VideoWriter::fourcc('M','J','P','G'),
								30,cv::Size(composite.cols,composite.rows),true);
					recorder_rows=composite.rows;
					recorder_cols=composite.cols;
					recording=true;

				}  else if(recording && ! start_recording) {
					vw.release();
					recording=false;
					start_recording=false;
				}				
				if (recording){
					if( composite.rows==recorder_rows &&
					     composite.cols==recorder_cols) {
					
					vw << composite;				
					cv::circle(composite,cv::Point(max(3,composite.cols/20),max(3,composite.rows/10)),max(5,composite.rows/100),CV_RGB(255,0,0),-1);
					} else {
						vw.release(); //if frame size is not compatible we restart the recording.
						recording=false;					
					}
				} 
				cv::imshow("composite", composite);
				
				double t2 = cv::getTickCount();
				//cout << "t: " <<(t2-t1)/cv::getTickFrequency() << endl;
				double render_time = 1000*(t2-t1)/cv::getTickFrequency();
			
			if (fps==0)
				c=cv::waitKey(3);
			else
				c=cv::waitKey(max(10.0,1000.0/fps-render_time));
			if (c == ' '){

				//BUFFERING
				double font_size = composite.rows*(0.025);
//				QFont font("arial", int(font_size));
//				QFontMetrics fm(font);
//				int res_width = fm.width("Buffering...");
//				int res_height = fm.height();
				int res_width = 500;
				int res_height = 100;
				cv::Rect text_background_roi(0,composite.rows-res_height*1.1,composite.cols,res_height*1.1);
				vector<cv::Mat> channels;
				cv::split(composite,channels);
				channels[0](text_background_roi)-=50;
				channels[1](text_background_roi)-=50;
				channels[2](text_background_roi)-=50;
				merge(channels,composite);
//				cv::addText(composite,"Buffering....",
//						cv::Point(0,composite.rows-composite.rows*.01),
//						"Arial",font_size,cv::Scalar(255,255,255));
				cv::imshow("composite",composite);
				cv::waitKey(3);				
				//////
				
				exit_loop=true;
				paused=false;
			} else if (c==27) {//press ESC to exit!
				if(recording) vw.release();//close recording to have a working file
				exit(0);
			} else if (c==122 || c==190){//F1 for help!
				show_help=true;
			} else if (c=='n' or c=='N'){
				paused = !paused;
			} else if (c=='r' or c=='R') {
				start_recording = !start_recording;			
			} else if (c=='d' or c=='D'){//toggle detections
				std::cout << "Det available: " << detections_available << std::endl;
				if (detections_available)
					detections_on = !detections_on;
			}
//			if (c>0)
//				cout << c << endl;

		}
	}

}
